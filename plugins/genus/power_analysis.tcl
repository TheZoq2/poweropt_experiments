puts "\nSCRIPT  : Starting power analysis.\n"

source ./session/$DESIGN_NAME.genus_setup.tcl

read_vcd $VCD_FILE

report_power > reports/power.vcd.txt


puts "\nSCRIPT  : Power analysis finished.\n"

exit
