# Library settings
set LIB_TRACKS "12";               # Number of tracks in the cell library
set LIB_TYPE "LL";                 # Cell library type, {LL | LR}
set LIB_CORE_VERSION "1";          # Core library version {0 | 1}
#set LIB_CLK_VERSION "5.1-03";      # Clock library version
#set LIB_PR_VERSION "5.3.a-00";     # Place and Route library version
set LIB_DETAILS "ss28_0.90V_125C"; # Library characterization details

# Technokit settings
set TECHNOKIT_VERSION "3.2.a-01"; # Technokit version

# Super thread settings
set SUPER_THREAD_CACHE_SIZE 32000; # Super thread servers chache size (Mb)
set SUPER_THREAD_SERVERS 8;       # Number of super thread servers to use
set SUPER_THREAD_DEBUG false;      # Write separate log file for each server

# Generic settings
set GENERIC_INFORMATION_LEVEL 11;  # {0 to 11}
set GENERIC_TRACK_FILENAMES true; # {true | false}
set GENERIC_HDL_LOOP_LIMIT 20000;

# Naming settings
set NAMING_USE_IF_GENERATE_PREFIX true;  # {true | false}
set NAMING_USE_FOR_GENERATE_PREFIX true; # {true | false}

# Timing (ns)
set TIMING_CLOCK_PORT "clk"
set TIMING_CLOCK_PERIOD 1.0
set TIMING_INPUT_DELAY 0
set TIMING_OUTPUT_DELAY 0

# General synthesis settings
set SYNTH_AUTO_UNGROUP both;   # {none | both}
set SYNTH_GENERIC_EFFORT high; # {high | low | medium | express | none}
set SYNTH_MAP_EFFORT high;     # {high | low | medium | express | none}
set SYNTH_OPT_EFFORT high;     # {high | low | medium | express | none}

# Physical flow settings (synth-to-placed)
set PHYSICAL_ENABLE false
set PHYSICAL_EFFORT high
set PHYSICAL_UTILIZATION 0.65

# Clock gating
set CLOCK_GATING_ENABLE true; # {true | false}

# Retiming
set RETIMING_ENABLE true;   # {true | false}
set RETIMING_DONT_RETIME {}; # List of registers to avoid moving when retiming

# List of cells to avoid.
set CELLS_AVOID {"*SDFP*"}

# Multi cycle paths (list of 3 element lists {"from_inst" "to_inst" cycles})
set MULTI_CYCLE_PATHS {}
