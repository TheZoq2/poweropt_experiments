# Load settings from file
source $PROJECT_ROOT/config_synth.tcl

# Setup logging
set_db -quiet / .information_level $GENERIC_INFORMATION_LEVEL
set_db -quiet [get_db messages *] .max_print 10
set_db -quiet / .verification_directory_naming_style fv/%s
if {$SUPER_THREAD_DEBUG} {
    set_db -quiet / .super_thread_debug_directory st_debug
}

# Set paths
# We have libraries with "fdsoi_24", "fdsoi_27a", "lp_42"
set PATH_LIB_CORE "$PATH_LIB_BASE/C28SOI_SC_${LIB_TRACKS}_CORE_${LIB_TYPE}@2.2@20131004.0"
set PATH_LIB_CLK  "$PATH_LIB_BASE/C28SOI_SC_${LIB_TRACKS}_CLK_${LIB_TYPE}@2.2@20131004.0"
set PATH_LIB_PR   "$PATH_LIB_BASE/C28SOI_SC_${LIB_TRACKS}_PR_${LIB_TYPE}@2.1@20131028.0"
set PATH_TECHNOKIT "$PATH_LIB_BASE/CadenceTechnoKit_cmos028FDSOI_6U1x_2U2x_2T8x_LB_LowPower@1.0.1@20121114.0"
# 
# Setup super threading
set_db / .auto_partition true
set_db / .max_super_thread_cache_size $SUPER_THREAD_CACHE_SIZE
set_db / .max_cpus_per_server $SUPER_THREAD_SERVERS

# Create list of LIB files
set LIB_FILES {}

#C28SOI_SC_12_CORE_LL_ss28_0.90V_0.00V_0.00V_0.00V_125C
lappend LIB_FILES $PATH_LIB_CORE/libs/C28SOI_SC_12_CORE_LL_ss28_0.90V_0.00V_0.00V_0.00V_125C.lib
lappend LIB_FILES $PATH_LIB_CLK/libs/C28SOI_SC_12_CLK_LL_ss28_0.90V_0.00V_0.00V_0.00V_125C.lib
lappend LIB_FILES $PATH_LIB_PR/libs/C28SOI_SC_12_PR_LL_ss28_0.90V_0.00V_0.00V_0.00V_125C.lib

#lappend LIB_FILES $PATH_LIB_CORE/libs/C32_SC_${LIB_TRACKS}_CORE_${LIB_TYPE}_${LIB_DETAILS}.lib
#lappend LIB_FILES $PATH_LIB_CLK/libs/C32_SC_${LIB_TRACKS}_CLK_${LIB_TYPE}_${LIB_DETAILS}.lib
#lappend LIB_FILES $PATH_LIB_PR/libs/C32_SC_${LIB_TRACKS}_PR_${LIB_DETAILS}.lib

# Create list of LEF files
set LEF_FILES {}
lappend LEF_FILES $PATH_TECHNOKIT/LEF/technology.12T.lef
lappend LEF_FILES $PATH_TECHNOKIT/LEF/viarule_generate.lef
lappend LEF_FILES $PATH_LIB_BASE/SiteDefKit_cmos28@1.4@20120720.0/LEF/sites.lef
lappend LEF_FILES $PATH_LIB_BASE/SiteDefKit_cmos28@1.4@20120720.0/LEF/lib_property.lef
lappend LEF_FILES $PATH_LIB_CORE/CADENCE/LEF/C28SOI_SC_${LIB_TRACKS}_CORE_${LIB_TYPE}_soc.lef
lappend LEF_FILES $PATH_LIB_CLK/CADENCE/LEF/C28SOI_SC_${LIB_TRACKS}_CLK_${LIB_TYPE}_soc.lef
lappend LEF_FILES $PATH_LIB_PR/CADENCE/LEF/C28SOI_SC_${LIB_TRACKS}_PR_${LIB_TYPE}_soc.lef


# Create list of QRC files
set QRC_FILES {}
#lappend QRC_FILES $PATH_TECHNOKIT/QRC_TECHFILE/FuncCmax.tech
#lappend QRC_FILES $PATH_TECHNOKIT/QRC_TECHFILE/FuncCmin.tech
lappend QRC_FILES $PATH_TECHNOKIT/QRC_TECHFILE/FuncRCmax.tech
#lappend QRC_FILES $PATH_TECHNOKIT/QRC_TECHFILE/FuncRCmin.tech
#lappend QRC_FILES $PATH_TECHNOKIT/QRC_TECHFILE/nominal.tech

# Setup design libraries
set_db / .library $LIB_FILES
set_db / .lef_library $LEF_FILES
set_db / .qrc_tech_file $QRC_FILES

# Netlist naming settings
set_db / .hdl_use_if_generate_prefix $NAMING_USE_IF_GENERATE_PREFIX
set_db / .hdl_use_for_generate_prefix $NAMING_USE_FOR_GENERATE_PREFIX
set_db / .hdl_array_naming_style %s_%d_
set_db / .hdl_generate_separator "___"
set_db / .hdl_generate_index_style %s_G_%d_
set_db / .hdl_interface_separator "__"

# Other elaboration settings
set_db / .hdl_track_filename_row_col $GENERIC_TRACK_FILENAMES
set_db / .hdl_max_loop_limit $GENERIC_HDL_LOOP_LIMIT

# Clock gating
if {$CLOCK_GATING_ENABLE} {
    set_db / .lp_insert_clock_gating true
}


# Read source files
read_hdl -sv $BUILD_DIR/spade.sv

# Elaborate
puts "\nSCRIPT  : Starting elaboration at: [clock format [clock seconds] -format {%x %X}].\n"
elaborate $TOP
puts "\nSCRIPT  : Elaboration finished at: [clock format [clock seconds] -format {%x %X}]."
puts "SCRIPT  :   Runtime:      [get_db / .real_runtime] s"
puts "SCRIPT  :   Memory usage: [get_db / .memory_usage] MB\n"

# # Synthesis settings
set_db / .auto_ungroup $SYNTH_AUTO_UNGROUP

# # Clock gating settings
if {$CLOCK_GATING_ENABLE} {
    set_db [get_lib_cells *CNHLS*_P0] .dont_use false
    set_db / .lp_clock_gating_prefix "CLKGATE"
}

# # Setup timing
create_clock -name $TIMING_CLOCK_PORT -period $TIMING_CLOCK_PERIOD [get_ports $TIMING_CLOCK_PORT]
# #set_input_delay -clock [get_clocks $TIMING_CLOCK_PORT] -name InputDelay $TIMING_INPUT_DELAY [get_ports -filter "direction==in && name != clk"]
# #set_output_delay -clock [get_clocks $TIMING_CLOCK_PORT] -name OutputDelay $TIMING_OUTPUT_DELAY [get_ports -filter "direction==out"]

# # Retiming
if {$RETIMING_ENABLE} {
    set_db [get_design design:*] .retime true
    foreach e $RETIMING_DONT_RETIME {
        set_db [get_db insts $e*] .dont_retime true
    }
}

# # Set multi sycle paths
foreach l $MULTI_CYCLE_PATHS {
    # Setup time
    set_multicycle_path -setup -from [get_db insts *[lindex $l 0]*] -to [get_db insts *[lindex $l 1]*] [lindex $l 2]
    # Hold time
    set_multicycle_path -hold - from [get_db insts *[lindex $l 0]*] -to [get_db insts *[lindex $l 1]*] [expr [lindex $l 2] - 1]
}

# # Set cells to avoid
foreach l $CELLS_AVOID {
    #set_db [get_lib_cells $l] .avoid true
}

# # Syntesis settings
set_db / .syn_generic_effort $SYNTH_GENERIC_EFFORT
set_db / .syn_map_effort $SYNTH_OPT_EFFORT
set_db / .syn_opt_effort $SYNTH_MAP_EFFORT
if {$PHYSICAL_ENABLE} {
    set_db / .phys_checkout_innovus_license true
    set_db / .phys_flow_effort $PHYSICAL_EFFORT
    set_db [get_db designs] .utilization_threshold $PHYSICAL_UTILIZATION
    read_def -keep_all_instances $PATH_BASE/out/$DESIGN_NAME/fp/${DESIGN_NAME}_fp.def
}

# # Synthesis
puts "\nSCRIPT  : Starting generic synthesis at: [clock format [clock seconds] -format {%x %X}].\n"
if {$PHYSICAL_ENABLE} {
    syn_generic -physical
} else {
    syn_generic
}
puts "\nSCRIPT  : Generic synthesis finished at: [clock format [clock seconds] -format {%x %X}]."
puts "SCRIPT  :   Runtime:      [get_db / .real_runtime] s"
puts "SCRIPT  :   Memory usage: [get_db / .memory_usage] MB\n"

puts "\nSCRIPT  : Starting synthesis top mapped at: [clock format [clock seconds] -format {%x %X}].\n"
if {$PHYSICAL_ENABLE} {
    syn_map -physical
} else {
    syn_map
}
puts "\nSCRIPT  : Synthesis to mapped finished at: [clock format [clock seconds] -format {%x %X}]."
puts "SCRIPT  :   Runtime:      [get_db / .real_runtime] s"
puts "SCRIPT  :   Memory usage: [get_db / .memory_usage] MB\n"

# puts "\nSCRIPT  : Starting gate level optimization at: [clock format [clock seconds] -format {%x %X}].\n"
if {$PHYSICAL_ENABLE} {
    syn_opt -physical
    syn_opt -physical -incremental
} else {
    syn_opt
    syn_opt -incremental
}
puts "\nSCRIPT  : Gate level optimization finished at: [clock format [clock seconds] -format {%x %X}]."
puts "SCRIPT  :   Runtime:      [get_db / .real_runtime] s"
puts "SCRIPT  :   Memory usage: [get_db / .memory_usage] MB\n"

set RESULT_BASE "$PROJECT_ROOT/$TOP/"

puts "\nSCRIPT  : Starting generation of design files and reports at: [clock format [clock seconds] -format {%x %X}].\n"
write_hdl > $RESULT_BASE/synth.v
# #write_sdf -delimiter / -edges check_edge -no_escape -setuphold split -recrem split > $DESIGN_NAME.sdf.gz
write_sdf -delimiter / -no_escape -interconn interconnect -setuphold split -recrem split > $RESULT_BASE/synth.sdf.gz
write_spef -power > $RESULT_BASE/synth.spef
write_sdc > $RESULT_BASE/.sdc.gz
write_design -innovus -base_name session/$TOP

report_timing > $RESULT_BASE/reports/timing.txt
report_gates > $RESULT_BASE/reports/gates.txt
report_area > $RESULT_BASE/reports/area.txt
report_dp > $RESULT_BASE/reports/datapath.txt
report_power > $RESULT_BASE/reports/power.txt
report_sequential > $RESULT_BASE/reports/sequential.txt
if {$CLOCK_GATING_ENABLE} {
    report_clock_gating > $RESULT_BASE/reports/clock_gating.txt
}
puts "\nSCRIPT  : Output generation finished at: [clock format [clock seconds] -format {%x %X}]."
puts "SCRIPT  :   Runtime:      [get_db / .real_runtime] s"
puts "SCRIPT  :   Memory usage: [get_db / .memory_usage] MB\n"

# exit
exit
