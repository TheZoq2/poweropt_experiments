#!/bin/bash

if [[ -z $1 ]]; then
  echo "The top module must be specified"
  exit 1
fi

TOP=$1

if [[ -z $BUILD_DIR ]]; then
  echo "BUILD_DIR is not set"
  exit 1
fi
if [[ -z $PLUGIN_DIR ]]; then
  echo "PLUGIN_DIR is not set"
  exit 1
fi
if [[ -z $PROJECT_ROOT ]]; then
  echo "PROJECT_ROOT is not set"
  exit 1
fi

if ! which genus; then
  echo
  echo "Genus not found, module loaded?"
  echo "    try module load cadence/genus-21.10.000"
  echo
  exit 1
fi

mkdir -p "$BUILD_DIR/genus"

echo "Top is $TOP"

genus -overwrite -execute "
  set PLUGIN_DIR $PLUGIN_DIR;
  set PROJECT_ROOT $PROJECT_ROOT;
  set BUILD_DIR $BUILD_DIR;
  set TOP $TOP" -file "$PLUGIN_DIR/tcl/synth.tcl" \
  | rehl -g "^@file.*" -y "^Warning" -y "^WARNING" -r "^Error" -p "^Info"

echo "Inserting test vcd generation code"

VCD_HEADER='
    `ifdef COCOTB_SIM
    string __top_module;
    string __vcd_file;
    initial begin
        if (\$value\$plusargs("TOP_MODULE=%s", __top_module) && __top_module == "##TOP##" && \$value\$plusargs("VCD_FILENAME=%s", __vcd_file)) begin
            \$dumpfile (__vcd_file);
            \$dumpvars (0, ##TOP##);
        end
    end
    `endif'

VCD_HEADER=$(perl -pe "s/##TOP##/$TOP/" <<< "${VCD_HEADER}")

perl -pe "s/(module $TOP.*$)/\1\n$VCD_HEADER/" <<< "$(cat $TOP/synth.v)" > $TOP/synth_with_header.v
