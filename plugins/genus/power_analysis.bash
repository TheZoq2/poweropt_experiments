#!/bin/bash

if [[ -z "$1" ]]; then
  echo "The top module must be specified as the first argument"
  exit 1
fi
if [[ -z "$2" ]]; then
  echo "The vcd file must be specified as the second argument"
  exit 1
fi
if !  file "$2" > /dev/null; then
  echo "$2 does not exist"
  exit 1
fi
genus -overwrite -execute "set DESIGN_NAME $1; set VCD_FILE $2" -file "${PLUGIN_DIR}/power_analysis.tcl" \
  | rehl -g "^@file.*" -y "^Warning" -y "^WARNING" -r "^Error" -p "^Info"
