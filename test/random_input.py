#top = main::spade_top

from cocotb.clock import Clock
from spade import SpadeExt
from cocotb import cocotb
from cocotb.triggers import FallingEdge
import random

@cocotb.test()
async def test(dut):
    s = SpadeExt(dut) # Wrap the dut in the Spade wrapper
    clk = dut.clk

    await cocotb.start(Clock(
        clk,
        period=10,
        units='ns'
    ).start())

    for _ in range(0, 2000):
        if random.randint(0, 2):
            s.i.s1_in = "StreamEna$(ena: false, data: 0)"
        else:
            s.i.s1_in = f"StreamEna$(ena: true, data: {random.randint(0, 128)})"
        if random.randint(0, 2):
            s.i.s2_in = "StreamEna$(ena: false, data: 0)"
        else:
            s.i.s2_in = f"StreamEna$(ena: true, data: {random.randint(0, 128)})"
        await FallingEdge(clk)

