#top = main::main

import random

from cocotb.clock import Clock
from spade import SpadeExt
from cocotb.triggers import FallingEdge
from cocotb import cocotb

@cocotb.test()
async def test(dut):
    s = SpadeExt(dut) # Wrap the dut in the Spade wrapper

    clk = dut.clk

    await cocotb.start(Clock(
        clk,
        period=10,
        units='ns'
    ).start())

    for i in range(0, 1000):
        s.i.op = "true" if random.randint(0, 1) else "false"
        s.i.opa = f"{random.randint(0, 2**32-1)}"
        s.i.opb = f"{random.randint(0, 2**32-1)}"
        await FallingEdge(clk)
